package tienda.online;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class Crearproducto extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		String nombre1 = req.getParameter("nombre1");
		String info = req.getParameter("info");
		String cat = req.getParameter("cat");
		int precio = Integer.parseInt(req.getParameter("precio"));
		int stock = Integer.parseInt(req.getParameter("stock"));
		String rutaImagen = "images/"+ req.getParameter("rutaImagen");
		
		
		Producto proc = new Producto(nombre1, info,cat,precio,stock,rutaImagen);
		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		try{
			pm.makePersistent(proc);
			RequestDispatcher rd = req.getRequestDispatcher("/administrador.jsp");
			rd.forward(req, resp);
			
		}catch(Exception e){
			System.out.println(e);
			resp.getWriter().println("Ocurri� un error, <a href='index.html'>Vuelva a intentarlo</a>");
		}finally{
			pm.close();
		}
	}
}
