package tienda.online;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

@SuppressWarnings("serial")
public class Servefoto extends HttpServlet {
	 private BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();

	 @Override
	 public void doGet(HttpServletRequest req, HttpServletResponse res)
	     throws IOException {
	         BlobKey blobKey = new BlobKey(req.getParameter("blob-key"));
	         blobstoreService.serve(blobKey, res);
	     }
	
         //blobstoreService.createGsBlobKey("blob-key");
         //String uploadUrl = blobstoreService.createUploadUrl("/agregarfoto");
         //req.setAttribute("uploadUrl", uploadUrl);
         //req.getRequestDispatcher("/agregarfoto.jsp").forward(req, res);
     }
 
 

