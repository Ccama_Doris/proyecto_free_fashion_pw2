package tienda.online;
import java.util.List;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class Productoutil {
//Cuantos datos mostrara en la consulta
private final static int FETCH_MAX_RESULTS = 40;
static PersistenceManager pm = PMF.get().getPersistenceManager();
//Metodo para insertar un dato
public static void insertarProducto(String nombre1, String info,String cat,int precio, int stock,String rutaImagen ){
 //LLamamos a la clase que tendra la persistencia
 //Creamos nuestra variable del tipo Persona
 final Producto produc= new Producto(nombre1, info,cat,precio,stock,rutaImagen);
 //Y hacemos el dato que sea persistente
 pm.makePersistent(produc);
}

//Para consultar los tutoriales por nombres
@SuppressWarnings("unchecked")
public static List<Producto> tutorialesPorNombres(String nombre1){
 final PersistenceManager pm = PMF.get().getPersistenceManager();
 String query = " select from " +
 Persona.class.getName() +
 " where nombre == '" +
 nombre1 + "'";
 List<Producto> nombres = (List<Producto>)pm.newQuery(query).execute();
 return(nombres);
}

//Consulta para los 
@SuppressWarnings("unchecked")
public static List<Producto> tutorialesPorId(long id){
 final PersistenceManager pm = PMF.get().getPersistenceManager();
 String query = " select from " +
 Persona.class.getName() +
 " where id == '" +
 id + "'";
 List<Producto> ids = (List<Producto>)pm.newQuery(query).execute();
 return(ids);
}


//Para consulta todos los alumnos
@SuppressWarnings("unchecked")
public static List<Producto> todosPersonas(){
 final PersistenceManager pm = PMF.get().getPersistenceManager();
 final Query query = pm.newQuery(Producto.class);
 query.setRange(0, FETCH_MAX_RESULTS);
 return (List<Producto>) query.execute();
}

//Para consulta todos los productos
@SuppressWarnings("unchecked")
public static List<Producto> todosProductos(){
final PersistenceManager pm = PMF.get().getPersistenceManager();
final Query query = pm.newQuery(Producto.class);
query.setRange(0, FETCH_MAX_RESULTS);
return (List<Producto>) query.execute();
}

public static void EliminarTodo(Persona persona) {
	pm.deletePersistent(persona);
}
}
