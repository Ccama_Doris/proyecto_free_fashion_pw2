package tienda.online;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import com.google.appengine.api.datastore.Key;

@PersistenceCapable
public class Producto {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key key;
	
	@Persistent
	private String nombre1;	
	@Persistent
	private String info;
	@Persistent
	private int precio;	
	@Persistent
	private int stock;	
	@Persistent
	private String cat;
	@Persistent
	private String rutaImagen;

	public Producto(String nombre1, String info,String cat,int precio, int stock,String rutaImagen ) {
		super();
		this.nombre1 = nombre1;
		this.info=info;
		this.precio = precio;
		this.stock = stock;
		this.cat = cat;
		this.rutaImagen=rutaImagen;
	}	
	
	public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}

	public String getNombre1() {
		return nombre1;
	}

	public void setNombre(String nombre1) {
		this.nombre1 = nombre1;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

	public String getRutaImagen() {
		return rutaImagen;
	}

	public void setRutaImagen(String rutaImagen) {
		this.rutaImagen = rutaImagen;
	}

	@Override
	public String toString() {
		String resp = nombre1 + " " + precio + " " + stock + " unidades disponibles.";  
		return resp;
	}
}


